# Themys

[Themys](https://gitlab.com/themys/themys) is a custom application based on the open-source data analysis and visualization application [ParaView](https://www.paraview.org/)
from [KitWare](https://www.kitware.com). It is a **ParaView's branded application**.

**Themys** depends on three functional projects:

- [Themys](https://gitlab.com/themys/themys): holds the specific GUI customization and documentation ;
- [Readers](https://gitlab.com/themys/readers): holds different readers that handle specific databases such as [Hercule](https://gitlab.com/hercule-io/public/hercule) ones ;
- [ThemysServerPlugins](https://gitlab.com/themys/themysserverplugins): is a collection of filters that respond to specific needs of our users.

**Themys** also relies on tool projects that enhance the quality of our software:

- [DockerImageFactory](https://gitlab.com/themys/dockerimagefactory): the CI of this project generates a Docker image that is itself used in [Readers](https://gitlab.com/themys/readers) and [ThemysServerPlugins](https://gitlab.com/themys/themysserverplugins) CI's ;
- [GlobalIntegrationTests](https://gitlab.com/themys/globalintegrationtests): the CI of this project build [Themys](https://gitlab.com/themys/themys), [ThemysServerPlugins](https://gitlab.com/themys/themysserverplugins), [Readers](https://gitlab.com/themys/readers) and then launch tests that involve the three projects ;
- [SpackInstaller](https://gitlab.com/themys/spackinstaller): holds [Spack](https://spack.readthedocs.io/en/latest/) recipes and environment that are used to install [Themys](https://gitlab.com/themys/themys), [ThemysServerPlugins](https://gitlab.com/themys/themysserverplugins) and [Readers](https://gitlab.com/themys/readers) on HPC clusters ;
- [Support](https://gitlab.com/themys/support): holds issues opened by our users that are not yet dispatched to any of the projects of the **Themys** group ;
- [ThemysCommonTools](https://gitlab.com/themys/themyscommontools): holds [CMake](https://cmake.org/) scripts and CI jobs that are common to some **Themys** projects ;
- [ThemysSuperBuild](https://gitlab.com/themys/themyssuperbuild): builds **Themys** relocatable binary installation. It is based on [ParaView's](https://www.paraview.org/) superbuild system ;
- [gitlab-profile](https://gitlab.com/themys/gitlab-profile): holds this file.